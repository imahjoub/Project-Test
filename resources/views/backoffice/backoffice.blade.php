<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> @yield('title', 'Laravel Test') </title>

    <!-- Bootstrap -->
    <link href="/backoffice/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/backoffice/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/backoffice/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="/backoffice/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>

    @yield('extra-css')

            <!-- Custom Theme Style -->
    <link href="/backoffice/build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">

        @include('backoffice.partials.menu')

        @include('backoffice.partials.topnavigation')

        @yield('content')


    </div>
</div>

<!-- jQuery -->
<script src="/backoffice/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="/backoffice/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="/backoffice/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="/backoffice/vendors/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script src="/backoffice/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

@yield('extra-js')

        <!-- Custom Theme Scripts -->
<script src="/backoffice/build/js/custom.min.js"></script>
</body>
</html>