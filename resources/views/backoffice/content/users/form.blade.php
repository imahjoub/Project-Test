
    <div class="item form-group {{ $errors->has('username') ? 'bad' : '' }}">
        {!! Form::label('User Name', null, ['class' => 'control-label col-md-1 col-sm-1 col-xs-12']) !!}
        <div class="col-md-7 col-sm-7 col-xs-12">
            {!! Form::text('username', null, ['class' => 'form-control','placeholder' => 'user name','required'=> 'required',  'data-validate-length-range' => '6']) !!}
        </div>
        @if($errors->has('username'))
             <div class="alert">{{ $errors->first('username') }}</div>
        @endif
    </div>

    <div class="item form-group {{ $errors->has('email') ? 'bad' : '' }}">
        {!! Form::label('Email', null, ['class' => 'control-label col-md-1 col-sm-1 col-xs-12']) !!}
        <div class="col-md-7 col-sm-7 col-xs-12">
            {!! Form::text('email', null, ['class' => 'form-control','placeholder' => 'email', 'required'=> 'required']) !!}
        </div>
        @if($errors->has('email'))
            <div class="alert">{{ $errors->first('email') }}</div>
        @endif
    </div>

    <div class="item form-group {{ $errors->has('phone') ? 'bad' : '' }}">
        {!! Form::label('Phone', null, ['class' => 'control-label col-md-1 col-sm-1 col-xs-12']) !!}
        <div class="col-md-7 col-sm-7 col-xs-12">
            {!! Form::tel('phone', null, ['class' => 'form-control','placeholder' => 'phone', 'required'=> 'required']) !!}
        </div>
        @if($errors->has('phone'))
            <div class="alert">{{ $errors->first('phone') }}</div>
        @endif
    </div>


    <div class="item form-group {{ $errors->has('password') ? 'bad' : '' }}">
        {!! Form::label('Password', null, ['class' => 'control-label col-md-1 col-sm-1 col-xs-12']) !!}
        <div class="col-md-7 col-sm-7 col-xs-12">
            {!! Form::password('password', ['class' => 'form-control','placeholder' => 'password']) !!}
        </div>
        @if($errors->has('password'))
            <div class="alert">{{ $errors->first('password') }}</div>
        @endif
    </div>

    <div class="item form-group {{ $errors->has('password_confirmation') ? 'bad' : '' }}">
        {!! Form::label('Confirm Password', null, ['class' => 'control-label col-md-1 col-sm-1 col-xs-12']) !!}
        <div class="col-md-7 col-sm-7 col-xs-12">
            {!! Form::password('password_confirmation', ['class' => 'form-control','placeholder' => 'confirm password']) !!}
        </div>
        @if($errors->has('password_confirmation'))
            <div class="alert">{{ $errors->first('password_confirmation') }}</div>
        @endif
    </div>


        <div class="item form-group {{ $errors->has('account_status') ? 'bad' : '' }}">
            {!! Form::label('Account Status*', null, ['class' => 'control-label col-md-1 col-sm-1 col-xs-12']) !!}
            <div class="col-md-7 col-sm-7 col-xs-12">
                @if( (!$user->exists) || !( $user->exists && ($user->id == Auth::user()->id && $user->hasRole('admin'))) && !($user->exists && ($user->id == Auth::user()->id && $user->hasRole('super-admin')) ))
                    Confirmed: {!! Form::radio('account_status', 'confirmed' ,true,['class' => 'flat']) !!}
                    Not Confirmed: {!! Form::radio('account_status', 'not_confirmed',false, ['class' => 'flat']) !!}
                    Banned: {!! Form::radio('account_status', 'banned',false, ['class' => 'flat']) !!}
                @else
                    {!! Form::hidden('account_status', $user->account_status) !!}
                    <p class="form-control-static">{{$user->getUserStatus() }}</p>
                @endif
            </div>
            @if($errors->has('account_status'))
                <div class="alert">{{ $errors->first('account_status') }}</div>
            @endif
        </div>


    <div class="imageupload item form-group {{ $errors->has('image') ? 'has-error' : '' }}">
        {!! Form::label('image', null, ['class' => 'control-label col-md-1 col-sm-1 col-xs-12']) !!}

        <div class="col-md-7 col-sm-7 col-xs-12" >
            @if(isset($user->image))
                <div class="fileinput-new thumbnail" id="default-image" style="width: 200px; max-height: 200px;">
                    <img src="{{ ($user->image) ? url('/'.$user->image) : 'http://placehold.it/200x150&text=No+Image' }}" alt="...">
                </div>
            @endif
            <div class="file-tab panel-body">
                <label class="btn btn-default btn-file">
                    <span>Browse</span>
                    <!-- The file is stored here. -->
                    <input type="file" name="image" accept="image/*">
                </label>
                <button type="button" class="btn btn-default">Remove</button>
            </div>
            <div class="url-tab panel-body">
                <div class="input-group">
                    <input type="text" class="form-control hasclear" placeholder="Image URL">
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default">Submit</button>
                    </div>
                </div>
                <button type="button" class="btn btn-default">Remove</button>
                <!-- The URL is stored here. -->
                <input type="hidden" name="image-url">
            </div>
        </div>

        @if($errors->has('image'))
            <span class="help-block">{{ $errors->first('image') }}</span>
        @endif
    </div>

    <div class="ln_solid"></div>
    <div class="form-group">
        <div class="col-md-6 col-md-offset-1">
            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        </div>
    </div>