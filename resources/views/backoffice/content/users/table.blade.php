
<table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>id</th>
        <th>User Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Account Status</th>
        <th>Created At</th>
        <th style="width: 15%">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php $currentUser = auth()->user(); ?>
    @foreach( $users as $user)
        <tr>
            {!! Form::open(['method' => 'DELETE', 'route' => [  strtolower($userType).'.destroy', $user->id]]) !!}
                <td>#{{$user->id}} <img src="{{ $user->image ? url('/'.$user->image) : url('uploads/users/user.png')}}" class="avatar pull-right" title="{{$user->username}}" alt="{{$user->username}}"></td>
                <td>{{$user->username}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->phone}}</td>
                <td>{{$user->getUserStatus()}}</td>
                <td>{{$user->created_at}}</td>
                <td>
                    <a href="{{route(strtolower($userType).'.edit' , $user->id)}}" class="btn btn-info btn-xs "><i class="fa fa-pencil"></i> Edit </a>
                    <button onclick="return confirm('Are you sure?');" type="submit" class="btn btn-danger btn-xs">
                        <i class="fa fa-trash-o"></i> Delete
                    </button>
                </td>
            {!! Form::close() !!}
        </tr>
    @endforeach
    </tbody>
</table>