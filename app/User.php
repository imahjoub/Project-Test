<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use LaratrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getUserStatus(){
        $satuts_code = $this->account_status;
        switch ($satuts_code){
            case 'confirmed':
                $status ='Confirmed';
                break;
            case 'not_confirmed':
                $status = 'Not Confirmed';
                break;
            case 'banned':
                $status = 'Banned';
                break;
            default:
                $status = 'Banned';
        }

        return $status;
    }
}
