<?php

namespace App\Http\Controllers\Backoffice;

use App\User;
use App\Http\Requests;

class UsersController extends BackofficeController
{
    private $userType;

    public function __construct()
    {
        $this->userType = "Users";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // \DB::enableQueryLog();
        $users      = User::all();
        $userType = $this->userType;
        // view("backoffice.content.users.index", compact('users','userType'))->render();
        return view("backoffice.content.users.index", compact('users','userType'))->render();
        //dd(\DB::getQueryLog());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User();
        $userType = $this->userType;
        return view("backoffice.content.users.create", compact('user','userType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\Backoffice\UserStoreRequest $request)
    {
        $data = $this->handleRequest($request);
        $user = User::create($data);
        $user->attachRole('cycled');

        return redirect(route('users.index'))->with("success", "New cycled was created successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $userType = $this->userType;
        return view("backoffice.content.users.edit", compact('user','userType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\Backoffice\UserUpdateRequest $request, $id)
    {

        $user = User::findOrFail($id);
        $data = $this->handleRequest($request, $user);
        $user->update($data);

        return redirect(route('users.index'))->with("success", "Cycled was updated successfully!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect(route('users.index'))->with("success", "Cycled was deleted successfully!");
    }
}
