<?php

namespace App\Http\Controllers\Backoffice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BackofficeController extends Controller
{


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect(route('users.index'));
    }

    protected $limit = 8;

    /*
     * generate random number
     */
    protected function generateRandomString($length) {

        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    protected function handleRequest($request, $user = null)
    {
        $data = $request->all();

        if ($request->hasFile('image'))
        {
            $image       = $request->file('image');
            $extension = $image->getClientOriginalExtension();
            $fileName    = $this->generateRandomString(12).'.'.$extension;
            $destination = public_path(config('customConfig.imageUsers.directory'));

            $image->move($destination, $fileName);
            $image_path = config('customConfig.imageUsers.directory'). '/' . $fileName;
            $data['image'] = $image_path;

            if(isset($user)){
                $this->removeImage($user->image);
            }
        }

        return $data;
    }

    /*
     * Remove Image
     */
    private function removeImage($image)
    {
        if ( ! empty($image) && $image != config('customConfig.imageUsers.directory'). '/' .config('customConfig.imageUsers.default') )
        {
            $imagePath     = public_path( $image);

            if ( file_exists($imagePath) ) unlink($imagePath);
        }
    }
}
