<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * @SWG\Swagger(
 *   schemes={"http"},
 *   host=API_HOST_DEV,
 *   basePath="/api/v1",
 *   @SWG\Info(
 *     title="Cycled API",
 *     version="1.0.0"
 *   )
 * )
 */

class BaseController extends Controller
{
    //
}
