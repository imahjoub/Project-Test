<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0 ');
        DB::Table('users')->Truncate();

        $users = array();
        $account_status = array('confirmed' , 'not_confirmed', 'banned');
        $faker = Faker\Factory::create();

        for ($i =0; $i<1000 ; $i++){
            $dateTime = $faker->dateTimeThisMonth($max = 'now', $timezone = date_default_timezone_get());
            $users[] =  [
                'username' => $faker->userName,
                'email' => $faker->email,
                'phone' => $faker->e164PhoneNumber,
                'password' => bcrypt($faker->password),
                'account_status' => $account_status[rand(0, 2)],
                'created_at' => $dateTime ,
                'updated_at' => $dateTime ,
            ];
        }

        $users[] =  [
            'username' => 'imahjoub',
            'email' => 'imahjoub.med@gmail.com',
            'phone' => '+21624414474',
            'password' => bcrypt('secret'),
            'account_status' => 'confirmed',
            'created_at' => '2017-11-11 10:45:00' ,
            'updated_at' => '2017-11-11 10:45:00' ,
        ];

        DB::Table('users')->insert($users);

        /*$users =  [
            'username' => 'imahjoub',
            'email' => 'imahjoub.med@gmail.com',
            'phone' => '+21624414474',
            'password' => bcrypt('secret'),
            'account_status' => 'confirmed',
            'created_at' => '2017-11-11 10:45:00' ,
            'updated_at' => '2017-11-11 10:45:00' ,
        ];

        DB::Table('users')->insert($users);

        factory(App\User::class, 100)->create();
        factory(App\Airport::class, 5)->create();
        factory(App\Flight::class, 10)->create()->each(function ($flight){
            factory(App\Customer::class, 100)->make()->each(function ($customer) use ($flight) {
                $flight->passengers()->save($customer);
            });
        });*/
    }
}
