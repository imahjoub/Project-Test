<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0 ');
        DB::table('roles')->truncate();
        DB::table('role_user')->truncate();

        //create Super Admin Role
        $superAdmin = new Role();
        $superAdmin->name = "super-admin";
        $superAdmin->display_name = "Super-Admin";
        $superAdmin->save();

        //create Admin Role
        $admin = new Role();
        $admin->name = "admin";
        $admin->display_name = "Admin";
        $admin->save();

        //create Doctor Role
        $author = new Role();
        $author->name = "author";
        $author->display_name = "Author";
        $author->save();

        //Attach the roles
        $user1 = User::find(1);
        $user1->detachRole($superAdmin);
        $user1->attachRole($superAdmin);

        $user2 = User::find(2);
        $user2->detachRole($admin);
        $user2->attachRole($admin);

        $user3 = User::find(3);
        $user3->detachRole($author);
        $user3->attachRole($author);

    }
}
