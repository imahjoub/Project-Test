<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

/*
 $factory->define(App\User::class, function (Faker\Generator $faker) {

    $account_status = array('confirmed' , 'not_confirmed', 'banned');
    $dateTime = $faker->dateTimeThisMonth('now', date_default_timezone_get());
    return [
        'username' => $faker->userName,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->e164PhoneNumber,
        'password' =>  bcrypt($faker->password),
        'account_status' => $account_status[rand(0, 2)],
        'remember_token' => str_random(10),
        'created_at' => $dateTime ,
        'updated_at' => $dateTime ,
    ];

});

$factory->define(App\Airport::class, function (Faker\Generator $faker) {

    return [
        'iataCode' => $faker->randomNumber(5),
        'city' => $faker->city,
        'state' => $faker->stateAbbr
    ];
});

$factory->define(App\Flight::class, function (Faker\Generator $faker) {

    return [
        'flightNumber' => str_random(5).$faker->unique()->randomNumber(5),
        'arrivalAirport_id' => $faker->numberBetween(1,5),
        'arrivalDateTime' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+10 Hours '),
        'departureAirport_id' => $faker->numberBetween(1,5),
        'departureDateTime' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+10 Hours '),
        'status' => $faker->boolean ? "ontime" : "delayed",
    ];
});

$factory->define(App\Customer::class, function (Faker\Generator $faker) {

    return [
        'firstName' => $faker->firstName(),
        'lastName' => $faker->lastName(),
    ];
});
*/