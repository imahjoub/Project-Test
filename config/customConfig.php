<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 29/06/2017
 * Time: 10:42
 */
define("API_HOST_DEV", "localhost:8000");

return [
    'imageUsers' => [
        'directory' => 'uploads/users',
        'default' => 'user.png',
        'thumbnail' => [
            'width' => 250,
            'height' => 170
        ]
    ]
];